#pragma once
#ifndef b
#define b


#include "Header.h"
#include <fstream>
std::fstream in("data.txt");

Data insertData()
{
    Data data;
    in >> data.day;
    in >> data.month;
    in >> data.amount;
    in >> data.characteristic;

    return data;
}
#endif

#pragma once
#ifndef a
#define a


#include <string>
using namespace std;


struct Data
{
    string day;
    string month;
    double amount = 0;
    string characteristic;
};
#endif